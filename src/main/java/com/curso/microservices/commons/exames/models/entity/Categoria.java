package com.curso.microservices.commons.exames.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "categorias")
public class Categoria implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	@JsonIgnoreProperties(value = {"associadas"})
	@ManyToOne(fetch = FetchType.LAZY)
	private Categoria principal;
	
	@JsonIgnoreProperties(value = {"principal"}, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "principal", cascade = CascadeType.ALL)
	private List<Categoria> associadas;

	public Categoria() {
		this.associadas = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Categoria getPrincipal() {
		return principal;
	}

	public void setPrincipal(Categoria principal) {
		this.principal = principal;
	}

	public List<Categoria> getAssociadas() {
		return associadas;
	}

	public void setAssociadas(List<Categoria> associadas) {
		this.associadas = associadas;
	}

}
